# docker build -t adoo1/serveone:latest .

FROM nginx:alpine

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/www.adooone.com.crt /etc/nginx/www.adooone.com.crt
COPY config/www.adooone.com.key /etc/nginx/www.adooone.com.key

# ADD config/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80 443

# BUILDING DOCKER
# docker build -t adoo1/serveone:latest .
# docker push adoo1/serveone:latest
# docker run -p 80:80 --name test adoo1/serveone

# docker run -p 80:80 -v ..frontone/dist:/sites --name test adoo1/serveone